/* $Id$
 *
 * Lasso - A free implementation of the Liberty Alliance specifications.
 *
 * Copyright (C) 2004-2007 Entr'ouvert
 * http://lasso.entrouvert.org
 *
 * Authors: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LASSO_PROVIDER_PRIVATE_H__
#define __LASSO_PROVIDER_PRIVATE_H__

#include <./serverprivate.h>
#include "../xml/private.h"
#include "../xml/saml-2.0/saml2_attribute.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/**
 * LassoPublicKeyType:
 * @LASSO_PUBLIC_KEY_SIGNING: Signing public key
 * @LASSO_PUBLIC_KEY_ENCRYPTION: Encryption public key
 *
 * Public key type.
 */
typedef enum {
	LASSO_PUBLIC_KEY_SIGNING,
	LASSO_PUBLIC_KEY_ENCRYPTION
} LassoPublicKeyType;

/* This structure should allow to map ID-FFv1.2 and SAMLv2 endpoints */
typedef struct _EndpointType {
	LassoProviderRole role;
	char *kind;
	char *binding;
	char *url;
	char *return_url;
	int index;
	int is_default;
} EndpointType;


struct _LassoProviderPrivate
{
	gboolean dispose_has_run;

	LassoProviderRole roles;
	LassoProtocolConformance conformance;
	GHashTable *Descriptors;
	GList *attributes; /* of LassoSaml2Attribute */
	char *default_assertion_consumer;
	xmlNode *organization;

	char *affiliation_owner_id;
	char *affiliation_id;

	GList *signing_public_keys;
	GList *signing_key_descriptors;
	xmlNode *encryption_key_descriptor;
	char *encryption_public_key_str;
	GList *encryption_public_keys;
	LassoEncryptionMode encryption_mode;
	LassoEncryptionSymKeyType encryption_sym_key_type;
	LassoKeyEncryptionMethod key_encryption_method;
	char *valid_until;
	char *cache_duration;
	GList *endpoints; /* of EndpointType_s */
	LassoSignatureContext signature_context;
};

gboolean lasso_provider_load_metadata(LassoProvider *provider, const gchar *metadata);
gboolean lasso_provider_load_metadata_from_buffer(LassoProvider *provider, const gchar *metadata);
gboolean lasso_provider_load_public_key(LassoProvider *provider,
		LassoPublicKeyType public_key_type);
GList* lasso_provider_get_public_keys(const LassoProvider *provider);
xmlSecKey* lasso_provider_get_encryption_public_key(const LassoProvider *provider);
LassoEncryptionSymKeyType lasso_provider_get_encryption_sym_key_type(const LassoProvider* provider);
LassoKeyEncryptionMethod lasso_provider_get_key_encryption_method(const LassoProvider* provider);
int lasso_provider_verify_query_signature(LassoProvider *provider, const char *message);
void _lasso_provider_load_key_descriptor(LassoProvider *provider, xmlNode *key_descriptor);
void _lasso_provider_add_metadata_value_for_role(LassoProvider *provider,
		LassoProviderRole role, const char *name, const char *value);
LassoProvider* lasso_provider_new_from_xmlnode(LassoProviderRole role, xmlNode *node);
gboolean lasso_provider_is_init(LassoProvider *provider);
const LassoSignatureContext *lasso_provider_get_signature_context(LassoProvider *provider);
const char *lasso_provider_get_affiliation_owner_id(LassoProvider *provider);
void lasso_provider_release_affiliation_owner_id(LassoProvider *provider);
void lasso_provider_set_affiliation_owner_id(LassoProvider *provider, const char *affiliation_owner_id);
void lasso_provider_set_affiliation_id(LassoProvider *provider, const char *affiliation_id);
void lasso_provider_set_valid_until_from_prop(LassoProvider *provider, xmlNode *node, xmlChar *name, xmlChar *ns);
void lasso_provider_set_cache_duration_from_prop(LassoProvider *provider, xmlNode *node, xmlChar *name, xmlChar *ns);
const char *lasso_provider_get_default_assertion_consumer(LassoProvider *provider);
void lasso_provider_set_default_assertion_consumer(LassoProvider *provider, char *assertion_consumer);
void lasso_provider_sort_endpoints(LassoProvider *provider, GCompareFunc cmpfun);
void lasso_provider_add_endpoint(LassoProvider *provider, EndpointType *endpoint);
const GList *lasso_provider_get_endpoints(LassoProvider *provider);
void lasso_provider_add_attribute(LassoProvider *provider, LassoSaml2Attribute *attribute);
void lasso_provider_set_organization(LassoProvider *provider, xmlNode *organization);
void lasso_provider_set_roles(LassoProvider *provider, LassoProviderRole roles);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LASSO_PROVIDER_PRIVATE_H__ */
